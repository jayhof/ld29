# LucanusCrawlus

I made this game during the 48 hour Ludum Dare (#29). It's far from done and has many Bugs and half implemented Ideas. Still the dare was a blast and i learned a lot.

[Try the game](http://ld29.phpublic.de/)

*Tested only in Google Chrome*

## Tools used

- Sublime Text
- Photoshop
- Stackoverflow :)

## Lessons learned

- I'm not a Pixelartist
- Learn a game framework for the next dare
