var GameScene = function(game)
{
	this.player = null;
	this.world = null;
	this.camera = null;
	this.entities = new EntitiesManager();
	this.buffer = null;

	this.numEnemies = 0;
	this.spawnEnemiesNum = 3;

	var self = this;


	this.init = function(callback)
	{
		this.buffer = new Buffer(
			new Rectangle(window.game.canvas.width, window.game.canvas.height),
			new Rectangle(0,0)
		);

		this.world = new World(new Rectangle(768, 512));
		this.world.init(function()
		{
			var spawnPoint = self.world.findSpawn();

			self.player = new PlayerEntity(new Frame(
				new Vector(
					spawnPoint.x * self.world.options.tileSize.width,
					spawnPoint.y * self.world.options.tileSize.height
				),
				new Rectangle(40,40)
			));
			self.player.init();

			self.entities.addEntity(self.player);

			self.camera = new Camera(
				self.player.frame.origin,
				self.buffer.size
			);

			callback();
		});
	}


	this.update = function()
	{
		if (this.numEnemies < this.spawnEnemiesNum)
		{
			for (i = this.numEnemies; i < this.spawnEnemiesNum; i++)
			{
				var spawnPoint = self.world.findSpawn();
				var enemy = new EnemyEntity(new Frame(
					new Vector(
						spawnPoint.x * self.world.options.tileSize.width,
						spawnPoint.y * self.world.options.tileSize.height
					),
					new Rectangle(40, 40)
				));
				enemy.init();
				self.entities.addEntity(enemy);
				this.numEnemies += 1;
			}
		};

		this.world.update();
		this.entities.update();
	}


	this.draw = function(ctx)
	{
		this.buffer.clear();

		this.world.draw(this.buffer.ctx);
		this.entities.draw(this.buffer.ctx);

		ctx.drawImage(
			this.buffer.canvas,
			self.camera.viewport().origin.x,
			self.camera.viewport().origin.y,
			self.camera.viewport().size.width,
			self.camera.viewport().size.height,
			0,
			0,
			self.camera.viewport().size.width,
			self.camera.viewport().size.height
		);
	}


	this.die = function()
	{
		window.game.loadScene(window.game.scenes.menu());
		window.game.state.playing = false;
	}
}
