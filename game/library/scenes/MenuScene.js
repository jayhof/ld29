var MenuScene = function(game)
{
	this.buffer = null;

	var self = this;


	this.init = function(callback)
	{
		this.buffer = new Buffer(
			new Rectangle(window.game.canvas.width, window.game.canvas.height),
			new Rectangle(0,0)
		);

		this.focus = new Vector(this.buffer.size.width / 2, this.buffer.size.height / 2);

		// window.game.canvas.onmousemove = this.mouseMove;
		document.onkeydown = this.keyDown;
		callback();
	}


	this.update = function()
	{

	}


	this.draw = function(ctx)
	{
		this.buffer.clear();

		this.buffer.ctx.drawImage(window.game.resource.getImage('splash.png'), 0, 0);

		ctx.drawImage(this.buffer.canvas, 0, 0);
	}


	this.keyDown = function(event)
	{
		window.game.loadScene(window.game.scenes.game());
		window.game.state.level = 1;
		window.game.state.playing = true;
	}


	this.mouseMove = function(event)
	{
		var relX = parseInt(event.clientX - window.game.canvas.offsetLeft);
		var relY = parseInt(event.clientY - window.game.canvas.offsetTop);
	}
}
