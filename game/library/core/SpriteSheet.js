var SpriteSheet = function(image, animations)
{
	this.image = image;
	this.animations = animations;
	this.currentAnimation = null;
	this.currentAnimationName = null;

	this.buffer = document.createElement('canvas');
	this.bufferctx = this.buffer.getContext('2d');

	this.intervalID = null;

	var self = this;


	this.init = function()
	{
	}


	this.setAnimation = function(name, ctx)
	{
		if (this.animations[name] && name !== this.currentAnimationName) {
			this.currentAnimationName = name;
			this.currentAnimation = this.animations[this.currentAnimationName];

			if (!this.currentAnimation.frame) {
				this.currentAnimation.frame = 0;
			}

			this.buffer.width = this.currentAnimation.tile.width;
			this.buffer.height = this.currentAnimation.tile.height;

			window.clearInterval(this.intervalID);

			this.intervalID = window.setInterval(function() {
				self.tick();
			}, this.currentAnimation.speed);
		};
	}


	this.tick = function()
	{
		if (self.currentAnimation.frame < self.currentAnimation.sequence.length - 1) {
			self.currentAnimation.frame += 1;
		}else {
			if (self.currentAnimation.callback) {
				window.clearInterval(self.intervalID);
				self.currentAnimation.callback();
			};
			self.currentAnimation.frame = 0;
		}
	}


	this.currentFrame = function()
	{
		this.bufferctx.clearRect(0, 0, this.buffer.width, this.buffer.height);
		this.bufferctx.drawImage(
			this.image,
			this.currentAnimation.start.x + (this.currentAnimation.tile.width * this.currentAnimation.sequence[this.currentAnimation.frame]),
			this.currentAnimation.start.y,
			this.currentAnimation.tile.width,
			this.currentAnimation.tile.height,
			0,
			0,
			this.currentAnimation.tile.width,
			this.currentAnimation.tile.height
		);

		return this.buffer;
	}
}
