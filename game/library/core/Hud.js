var Hud = function(frame)
{
	this.frame = frame;
	this.buffer = null;


	this.init = function()
	{
		this.buffer = new Buffer(this.frame.size, new Rectangle(0,0));
	}


	this.update = function()
	{

	}


	this.draw = function(ctx)
	{
		this.buffer.clear();

		this.buffer.ctx.fillStyle = "rgba(0,0,0,0.3)";
		this.buffer.ctx.fillRect(0, 0, this.frame.size.width, this.frame.size.height);

		this.buffer.ctx.fillStyle = "rgba(100,100,100,0.6)";
		this.buffer.ctx.fillRect(0, 0, this.frame.size.width, 1);

		this.buffer.ctx.fillStyle = "rgb(200,0,0)";
		this.buffer.ctx.fillRect(10, 10, window.game.scene.player.health, 20);

		this.buffer.ctx.fillStyle = "rgb(255,255,255)";
		this.buffer.ctx.fillText("Kills: "+ window.game.state.kills, 140, 20);

		this.buffer.ctx.fillStyle = "rgb(255,255,255)";
		this.buffer.ctx.fillText("Score: "+ window.game.state.score, 240, 20);

		this.buffer.ctx.fillStyle = "rgb(255,255,255)";
		this.buffer.ctx.fillText("Level: "+ window.game.state.level, 340, 20);

		ctx.drawImage(
			this.buffer.canvas,
			this.frame.origin.x - (this.buffer.padding.width / 2),
			this.frame.origin.y - (this.buffer.padding.height / 2)
		);
	}
}
