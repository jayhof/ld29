var Camera = function(origin, size)
{
	this.origin = origin;
	this.size = size;


	this.init = function()
	{

	}

	this.viewport = function()
	{
		return new Frame(
			new Vector(
				this.origin.x - (this.size.width / 2),
				this.origin.y - (this.size.height / 2)
			),
			this.size
		);
	}
}
