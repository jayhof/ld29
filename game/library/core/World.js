var World = function(size)
{
	this.options = {
		name: 'World 1',
		imageSrc: 'world1.png',
		tileSize: new Rectangle(32,32),
		tiles: {
			floor: {
				position: [0,0],
				solid: false
			},
			corner_tl: {
				position: [1,0],
				solid: true
			},
			top: {
				position: [2,0],
				solid: true
			},
			corner_tr: {
				position: [3,0],
				solid: true
			},
			left: {
				position: [1,1],
				solid: true
			},
			hole: {
				position: [2,1],
				solid: true
			},
			right: {
				position: [3,1],
				solid: true
			},
			corner_bl: {
				position: [1,2],
				solid: true
			},
			bottom: {
				position: [2,2],
				solid: true
			},
			corner_br: {
				position: [3,2],
				solid: true
			},
			hole_corner_tl: {
				position: [4,0],
				solid: true
			},
			hole_top: {
				position: [5,0],
				solid: true
			},
			hole_corner_tr: {
				position: [6,0],
				solid: true
			},
			hole_left: {
				position: [4,1],
				solid: true
			},
			hole_center: {
				position: [5,1],
				solid: true
			},
			hole_center2: {
				position: [5,1],
				solid: true
			},
			hole_right: {
				position: [6,1],
				solid: true
			},
			hole_corner_bl: {
				position: [4,2],
				solid: true
			},
			hole_bottom: {
				position: [5,2],
				solid: true
			},
			hole_corner_br: {
				position: [6,2],
				solid: true
			}
		}
	};
	this.tileset = null;
	this.size = size;
	this.map = [];
	this.buffer = null;
	this.color = "rgb(200,0,0)";

	var self = this;


	this.init = function(callback)
	{
		var mainColor = Math.floor(Math.random() * 3) +1;
		var red = Math.floor(Math.random()*55);
		var green = Math.floor(Math.random()*55);
		var blue = Math.floor(Math.random()*55);

		if (mainColor == 1) {
			red = Math.floor(Math.random()*200+55);
		}
		if (mainColor == 2) {
			green = Math.floor(Math.random()*200+55);
		}
		if (mainColor == 3) {
			blue = Math.floor(Math.random()*200+55);
		}

		this.color = "rgba("+ red +","+ green +","+ blue +",0.5)";

		this.tileset = new Tileset(window.game.resource.getImage(this.options.imageSrc), this.options.tileSize);
		this.tileset.init();

		this.buffer = new Buffer(this.size, new Rectangle(0,0));

		var horizontalTiles = parseInt(this.size.width / this.options.tileSize.width);
		var verticalTiles = parseInt(this.size.height / this.options.tileSize.height);

		for (var x = 0; x < horizontalTiles; x++)
		{
			if (!this.map[x]) this.map[x] = [];

			for (var y = 0; y < verticalTiles; y++)
			{
				var frame = new Frame(
					new Vector(this.options.tileSize.width * x, this.options.tileSize.height * y),
					new Rectangle(this.options.tileSize.width, this.options.tileSize.height)
				);

				this.map[x][y] = this.options.tiles.floor;

				// if (Math.random() > .9) {
				// 	this.map[x][y] = this.options.tiles.hole;
				// }

				if (x == 0) {
					this.map[x][y] = this.options.tiles.left;
				}

				if (x == horizontalTiles - 1) {
					this.map[x][y] = this.options.tiles.right;
				}

				if (y == 0) {
					this.map[x][y] = this.options.tiles.top;
				}

				if (y == verticalTiles - 1) {
					this.map[x][y] = this.options.tiles.bottom;
				}

				if (x == 0 && y == 0) {
					this.map[x][y] = this.options.tiles.corner_tl;
				}

				if (x == 0 && y == verticalTiles - 1) {
					this.map[x][y] = this.options.tiles.corner_bl;
				}

				if (y == 0 && x == horizontalTiles - 1) {
					this.map[x][y] = this.options.tiles.corner_tr;
				}

				if (x == horizontalTiles - 1 && y == verticalTiles - 1) {
					this.map[x][y] = this.options.tiles.corner_br;
				}

				if (Math.random() > .99 && this.map[x][y] == this.options.tiles.floor) {
					this.map[x][y] = this.options.tiles.hole_center;
				}
			}
		}

		// create bigger holes
		for (var x = 0; x < this.map.length; x++)
		{
			for(var y =0; y< this.map[x].length; y++)
			{
				if (this.map[x][y] == this.options.tiles.hole_center)
				{
					var goodLocation = true;
					var changedTiles = [];
					changedTiles.push([x,y]);

					var radiusX = Math.floor((Math.random() * 10) / 2 + 1);
					var radiusY = Math.floor((Math.random() * 10) / 2 + 1);
					var growX = x + radiusX;
					var growY = y + radiusY;

					if (growX + 1 < this.map.length && growY + 1 < this.map[x].length)
					{
						for (var x2 = x; x2 < growX; x2++)
						{
							for (var y2 = y; y2 < growY; y2++)
							{
								if (this.map[x2][y2] = this.options.tiles.floor && goodLocation) {
									this.map[x2][y2] = this.options.tiles.hole_center2;
									changedTiles.push([x2,y2]);
								}else {
									goodLocation = false;
								}
							}
						}
						if (this.map[x-1][y-1] && this.map[x-1][y-1] == this.options.tiles.floor) {
							this.map[x-1][y-1] = this.options.tiles.hole_corner_tl;
							changedTiles.push([x-1,y-1]);
						}else {
							goodLocation = false;
						}
						if (this.map[growX][y-1] && this.map[growX][y-1] == this.options.tiles.floor) {
							this.map[growX][y-1] = this.options.tiles.hole_corner_tr;
							changedTiles.push([growX,y-1]);
						}else {
							goodLocation = false;
						}
						if (this.map[x-1][growY] && this.map[x-1][growY] == this.options.tiles.floor) {
							this.map[x-1][growY] = this.options.tiles.hole_corner_bl;
							changedTiles.push([x-1,growY]);
						}else {
							goodLocation = false;
						}
						if (this.map[growX][growY] && this.map[growX][growY] == this.options.tiles.floor) {
							this.map[growX][growY] = this.options.tiles.hole_corner_br;
							changedTiles.push([growX,growY]);
						}else {
							goodLocation = false;
						}
						// top neighbors
						for (var x2 = x; x2 < growX; x2++) {
							if (this.map[x2][y-1] && this.map[x2][y-1] == this.options.tiles.floor) {
								this.map[x2][y-1] = this.options.tiles.hole_top;
								changedTiles.push([x2,y-1]);
							}else {
								goodLocation = false;
							}
						}
						// bottom neighbors
						for (var x2 = x; x2 < growX; x2++) {
							if (this.map[x2][growY] && this.map[x2][growY] == this.options.tiles.floor) {
								this.map[x2][growY] = this.options.tiles.hole_bottom;
								changedTiles.push([x2,growY]);
							}else {
								goodLocation = false;
							}
						}
						// left neighbors
						for (var y2 = y; y2 < growY; y2++) {
							if (this.map[x-1][y2] && this.map[x-1][y2] == this.options.tiles.floor) {
								this.map[x-1][y2] = this.options.tiles.hole_left;
								changedTiles.push([x-1,y2]);
							}else {
								goodLocation = false;
							}
						}
						// right neighbors
						for (var y2 = y; y2 < growY; y2++) {
							if (this.map[growX][y2] && this.map[growX][y2] == this.options.tiles.floor) {
								this.map[growX][y2] = this.options.tiles.hole_right;
								changedTiles.push([growX,y2]);
							}else {
								goodLocation = false;
							}
						}
					}else {
						goodLocation = false;
					}

					// undo
					if (!goodLocation) {
						for (var i = changedTiles.length - 1; i >= 0; i--) {
							this.map[changedTiles[i][0]][changedTiles[i][1]] = this.options.tiles.floor;
						};
					}
				}
			}
		}

		// render map
		for (var x = 0; x < this.map.length; x++)
		{
			for(var y =0; y< this.map[x].length; y++)
			{
				this.buffer.ctx.drawImage(
					this.tileset.getTile(this.map[x][y].position),
					this.options.tileSize.width * x,
					this.options.tileSize.height * y
				);
			}
		}
		callback();
	}


	this.findSpawn = function()
	{
		var randomX = Math.floor(Math.random() * this.map.length);
		var randomY = Math.floor(Math.random() * this.map[randomX].length);

		while(this.map[randomX][randomY] !== this.options.tiles.floor) {
			randomX = Math.floor(Math.random() * this.map.length);
			randomY = Math.floor(Math.random() * this.map[randomX].length);
		}

		return new Vector(randomX,randomY);
	}


	this.findTile = function(position)
	{
		var x = Math.floor(position.x / this.options.tileSize.width);
		var y = Math.floor(position.y / this.options.tileSize.height);

		return this.map[x][y];
	}


	this.update = function()
	{

	}


	this.draw = function(ctx)
	{
		// this.buffer.ctx.globalCompositeOperation = 'darken';
		// this.buffer.ctx.fillStyle = this.color;
		// this.buffer.ctx.fillRect(0,0,this.buffer.canvas.width,this.buffer.canvas.height);

		ctx.drawImage(this.buffer.canvas, 0, 0);
	}

}
