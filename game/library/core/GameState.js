var GameState = function()
{
	this.playing = false;
	this.level = 0;
	this.kills = 0;
	this.score = 0;

	this.update = function()
	{
		if (this.playing)
		{
			if (Math.floor(this.score / this.level) >= 2)
			{
				this.level += 1;
				window.game.loadScene(window.game.scenes.game());
			}
		}
	}

	this.addKill = function(scoreMultiplier)
	{
		// update scenes enemy count
		window.game.scene.numEnemies -= 1;

		this.kills += 1;
		this.score += 1 * scoreMultiplier;
	}

}
