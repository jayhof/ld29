var ResourceManager = function()
{
	this.images = [];
	this.loadedImages = 0;

	var self = this;


	this.loadImages = function(imagePaths, callback)
	{
		for (var i = 0; i < imagePaths.length; i++)
		{
			this.images[i] = new Image;
			this.images[i].src = imagePaths[i];

			this.images[i].onload = function() {
				self.loadedImages += 1;
				if (self.loadedImages == imagePaths.length) {
					callback();
				};
			}
		}
	}


	this.getImage = function(imagePath)
	{
		for (var i = 0; i < this.images.length; i++)
		{
			if (this.images[i].src.indexOf(imagePath) > 0) {
				return this.images[i];
			}
		}

		return null;
	}
}
