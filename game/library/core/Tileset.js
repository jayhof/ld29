var Tileset = function(image, size)
{
	this.image = image;
	this.size = size;

	this.tiles = [];
	this.buffer = document.createElement('canvas');
	this.bufferctx = this.buffer.getContext('2d');

	var self = this;


	this.init = function()
	{
		this.buffer.width = this.size.width;
		this.buffer.height = this.size.height;
	}


	this.getTile = function(tileIndex)
	{
		this.bufferctx.clearRect(0, 0, this.buffer.width, this.buffer.height);

		this.bufferctx.drawImage(
			this.image,
			tileIndex[0] * this.size.width,
			tileIndex[1] * this.size.height,
			this.size.width,
			this.size.height,
			0,
			0,
			this.size.width,
			this.size.height
		);

		// var image = new Image;
		// image.src = this.buffer.toDataURL();

		// document.body.appendChild(image);

		return this.buffer;
	}
}
