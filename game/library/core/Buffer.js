var Buffer = function(size, padding)
{
	this.size = size;
	this.padding = padding;

	this.canvas = document.createElement('canvas');
	this.canvas.width = this.size.width + this.padding.width;
	this.canvas.height = this.size.height + this.padding.height;
	this.ctx = this.canvas.getContext('2d');


	this.clear = function() {
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	this.center = function() {
		return new Vector(this.canvas.width / 2, this.canvas.height / 2);
	}
}
