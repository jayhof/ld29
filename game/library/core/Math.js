var Vector = function(x, y)
{
	this.x = x;
	this.y = y;

	this.add = function(vector)
	{
		this.x += vector.x;
		this.y += vector.y;
	}

	this.substract = function(vector)
	{
		this.x -= vector.x;
		this.y -= vector.y;
	}
}



var Circle = function(center, radius)
{
	this.center = center;
	this.radius = radius;
}



var Rectangle = function(width, height)
{
	this.width = width;
	this.height = height;
}


var Frame = function(origin, size)
{
	this.origin = origin;
	this.size = size;

	this.center = function()
	{
		return new Vector(this.origin.x + this.size.width / 2, this.origin.y + this.size.height / 2);
	}

	this.radius = function()
	{
		return this.size.width / 2;
	}

	this.circleCollision = function(target)
	{
		var dx = target.center().x - this.center().x;
		var dy = target.center().y - this.center().y;
		var dist = Math.sqrt(dx*dx + dy*dy);

		if (dist < target.radius() + this.radius()) {
			return true;
		}

		return false;
	}
}
