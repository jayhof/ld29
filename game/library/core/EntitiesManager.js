var EntitiesManager = function()
{
	this.entities = [];
	this.newEntities = [];
	this.removedEntities = [];


	this.update = function()
	{
		this.updateEntities();

		for (i in this.entities) {
			if (this.entities[i].update)
			{
				// check for collisions
				if (this.entities[i].solid)
				{
					for (j in this.entities)
					{
						if (this.entities[j].solid && this.entities[i] !== this.entities[j])
						{
							if (this.entities[i].frame.circleCollision(this.entities[j].frame))
							{
								this.entities[i].collides(this.entities[j]);
							}
						}
					}
				}

				this.entities[i].update();
			}
		}
	}


	this.draw = function(ctx)
	{
		for (i in this.entities) {
			if (this.entities[i].draw) {
				this.entities[i].draw(ctx);

				// ctx.strokeStyle = "rgba(255,255,255,0.2)";
				// ctx.arc(
				// 	this.entities[i].frame.center().x,
				// 	this.entities[i].frame.center().y,
				// 	this.entities[i].frame.size.width / 2,
				// 	0,
				// 	Math.PI * 2
				// );
				// ctx.closePath();
				// ctx.stroke();
			}
		}
	}


	this.addEntity = function(entity)
	{
		this.newEntities.push(entity);
	}


	this.removeEntity = function(entity)
	{
		this.removedEntities.push(entity);
	}

	this.removeAllEntities = function()
	{
		this.removedEntities.push(this.entities);
	}


	this.updateEntities = function()
	{
		if (this.newEntities.length > 0) {
			for (i in this.newEntities) {
				this.entities.push(this.newEntities[i]);
			}
			this.newEntities = [];
		}

		if (this.removedEntities.length > 0) {
			for (i in this.removedEntities) {
				var index = this.entities.indexOf(this.removedEntities[i])
				this.entities.splice(index, 1);
			}
			this.removedEntities = [];
		}
	}
}
