var PlayerEntity = function(frame)
{
	this.frame = frame;
	this.solid = true;
	this.velocity = new Vector(0,0);
	this.speed = 1.5;
	this.rotation = 0;
	this.moving = false;
	this.playerSpriteSheet = null;
	this.weapon = null;
	this.player = true;
	this.health = 100;
	this.alive = true;

	this.buffer = null;

	var self = this;


	this.init = function()
	{
		this.buffer = new Buffer(this.frame.size, new Rectangle(30,30));

		this.playerSpriteSheet = new SpriteSheet(window.game.resource.getImage('player.png'), {
			idle: {
				tile: new Rectangle(40,40),
				start: new Vector(0,40),
				sequence: [0,1,2,1],
				speed: 500
			},
			walk: {
				tile: new Rectangle(40,40),
				start: new Vector(0,0),
				sequence: [0,1,2,3],
				speed: 150
			}
		});
		this.playerSpriteSheet.init();

		this.weapon = new Pliers(new Frame(
			new Vector(31,7),
			new Rectangle(26,26)
		));
		this.weapon.init();

		document.onkeydown = this.keyDown;
		document.onkeyup = this.keyUp;
		window.game.canvas.onmousemove = this.mouseMove;
		window.game.canvas.onmousedown = this.mouseDown;
		window.game.canvas.onmouseup = this.mouseUp;
	}


	this.update = function()
	{
		if (this.health <= 0)
		{
			this.alive = false;
			this.moving = false;

			window.game.scene.die();
		}

		if (this.moving)
		{
			// check for collisions
			var newOrigin = new Vector(this.frame.center().x, this.frame.center().y);
			newOrigin.add(this.velocity);

			var tile = window.game.scene.world.findTile(newOrigin);

			if (!tile.solid)
			{
				this.moving = true;
				this.frame.origin.add(this.velocity);
			}else {
				this.moving = false;
				this.velocity = new Vector(0,0);
			}
			this.playerSpriteSheet.setAnimation('walk');
		}else {
			this.playerSpriteSheet.setAnimation('idle');
		}

		// check for collision with enemies
		// if (this.weapon.attacking)
		// {
		// 	for (var i = window.game.scene.entities.entities.length - 1; i >= 0; i--) {
		// 		// if(window.game.scene.entities.entities[i] !== self && window.game.scene.entities.entities[i].collides(this.frame)) {
		// 		if(window.game.scene.entities.entities[i] !== self && window.game.scene.entities.entities[i].collidesCircle(this.frame)) {
		// 			self.health -= window.game.scene.entities.entities[i].damage;
		// 			window.game.scene.entities.entities[i].health -= this.weapon.damage;
		// 		}
		// 	};
		// }
	}


	this.draw = function(ctx)
	{
		this.buffer.clear();

		this.buffer.ctx.save();
		this.buffer.ctx.translate(this.buffer.center().x, this.buffer.center().y);
		this.buffer.ctx.rotate(this.rotation);

		this.buffer.ctx.drawImage(
			this.playerSpriteSheet.currentFrame(),
			(this.frame.size.width / 2) * -1,
			(this.frame.size.height / 2) * -1
		);

		this.buffer.ctx.translate(
			(this.buffer.center().x - (this.buffer.padding.width / 2)) * -1,
			(this.buffer.center().y - (this.buffer.padding.height / 2)) * -1
		);

		this.buffer.ctx.drawImage(
			this.weapon.spriteSheet.currentFrame(),
			this.weapon.frame.origin.x,
			this.weapon.frame.origin.y
		);

		// this.buffer.ctx.strokeStyle = '#ffc';
		// this.buffer.ctx.strokeRect(0,0,this.buffer.size.width, this.buffer.size.height);

		this.buffer.ctx.restore();

		ctx.drawImage(
			this.buffer.canvas,
			this.frame.origin.x - (this.buffer.padding.width / 2),
			this.frame.origin.y - (this.buffer.padding.height / 2)
		);

		// ctx.strokeStyle = "rgba(255,255,255,.5)";
		// ctx.arc(
		// 	this.frame.center().x + (this.weapon.frame.center().x) * Math.cos(this.rotation) - (this.weapon.frame.center().y) * Math.sin(this.rotation),
		// 	this.frame.center().y + (this.weapon.frame.center().x) * Math.sin(this.rotation) + (this.weapon.frame.center().y) * Math.cos(this.rotation),
		// 	this.weapon.frame.size.width / 2,
		// 	0,
		// 	Math.PI * 2
		// );
		// ctx.stroke();
		// this.frame.origin.x + (weaponGlobalSpace.x-this.frame.origin.x) * Math.cos(this.rotation) - (weaponGlobalSpace.y-this.frame.origin.y) * Math.sin(this.rotation),
		// 	this.frame.origin.y + (weaponGlobalSpace.x-this.frame.origin.x) * Math.sin(this.rotation) + (weaponGlobalSpace.y-this.frame.origin.y) * Math.cos(this.rotation),
		// this.frame.origin.x + Math.cos(this.weapon.frame.origin.x * this.rotation),
		// 	this.frame.origin.y + Math.sin(this.weapon.frame.origin.y * this.rotation),
	}


	this.keyDown = function(event)
	{
		// Move Forwards
		if(event.keyCode == 38 || event.keyCode == 87)
		{
			self.moving = true;
			self.velocity = new Vector(
				Math.round((self.speed * Math.cos(self.rotation)) * 100) / 100,
				Math.round((self.speed * Math.sin(self.rotation)) * 100) / 100
			);
		}

		// Move Backwards
		if (event.keyCode == 40 || event.keyCode == 83)
		{
			self.moving = true;
			self.velocity = new Vector(
				(Math.round((self.speed * Math.cos(self.rotation)) * 100) / 100) * -1,
				(Math.round((self.speed * Math.sin(self.rotation)) * 100) / 100) * -1
			);
		}
	}


	this.keyUp = function(event)
	{
		// Stop Moving
		if((event.keyCode == 38 || event.keyCode == 87 || event.keyCode == 40 || event.keyCode == 83) && self.moving) {
			self.moving = false;
			self.velocity = new Vector(0,0);
		}
	}


	this.mouseMove = function(event)
	{
		// var relX = parseInt(event.clientX - window.game.canvas.offsetLeft) - (self.frame.origin.x + self.frame.size.width / 2);
		// var relY = parseInt(event.clientY - window.game.canvas.offsetTop) - (self.frame.origin.y + self.frame.size.height / 2);
		var relX = parseInt(event.clientX - window.game.canvas.offsetLeft) - (window.game.canvas.width / 2);
		var relY = parseInt(event.clientY - window.game.canvas.offsetTop) - (window.game.canvas.height / 2);

		self.rotation = Math.atan2(relY, relX);
	}


	this.mouseDown = function(event)
	{
		self.weapon.startAttacking();
	}

	this.mouseUp = function(event)
	{
		self.weapon.stopAttacking();
	}

	this.collides = function(target)
	{
		if (target.health > 0 && this.weapon.attacking)
		{
			target.health = Math.floor(target.health - this.weapon.damage);

			// target died?
			if (target.health <= 0) {
				window.game.state.addKill(target.scoreMultiplier);
			}
		}
	}
}
PlayerEntity.prototype = new Entity;
