var Pliers = function(frame)
{
	this.frame = frame;
	this.attacking = false;
	this.damage = 0.5;
	this.spriteSheet = null;

	var self = this;


	this.init = function()
	{
		this.spriteSheet = new SpriteSheet(window.game.resource.getImage('weapons.png'), {
			idle: {
				tile: new Rectangle(26,26),
				start: new Vector(0,0),
				sequence: [0,1,2,1],
				speed: 500
			},
			attacking: {
				tile: new Rectangle(26,26),
				start: new Vector(0,26),
				sequence: [0,1,2,3,2,1],
				speed: 100
			}
		});
		this.spriteSheet.init();

		this.spriteSheet.setAnimation('idle');
	}

	this.startAttacking = function()
	{
		if (!this.attacking) {
			this.attacking = true;
			this.spriteSheet.setAnimation('attacking');
		}
	}

	this.stopAttacking = function()
	{
		if (this.attacking) {
			this.attacking = false;
			this.spriteSheet.setAnimation('idle');
		}
	}

	this.update = function()
	{
	}
}
Pliers.prototype = new Entity;
