var EnemyEntity = function(frame)
{
	this.frame = frame;
	this.solid = true;
	this.enemySpriteSheet = null;
	this.speed = 0.25;
	this.rotation = 0;
	this.health = 100;
	this.alive = true;
	this.damage = 0.1;
	this.scoreMultiplier = 1;
	this.moving = false;
	this.distance = null;
	this.activationDistance = 300;

	this.buffer = null;

	var self = this;


	this.init = function()
	{
		this.buffer = new Buffer(this.frame.size, new Rectangle(10,10));

		this.enemySpriteSheet = new SpriteSheet(window.game.resource.getImage('enemy1.png'), {
			idle: {
				tile: new Rectangle(40,40),
				start: new Vector(0,40),
				sequence: [0,1,2,1],
				speed: 500
			},
			walk: {
				tile: new Rectangle(40,40),
				start: new Vector(0,0),
				sequence: [0,1,2,3,2,1],
				speed: 250
			},
			die: {
				tile: new Rectangle(40,40),
				start: new Vector(0,80),
				sequence: [0,1,2,3],
				speed: 250,
				callback: function() {
					window.game.scene.entities.removeEntity(self);
				}
			}
		});
		this.enemySpriteSheet.init();

		this.enemySpriteSheet.setAnimation('walk');
	}


	this.update = function()
	{
		if (this.health <= 0) {
			this.alive = false;
		}

		if (!this.alive)
		{
			this.enemySpriteSheet.setAnimation('die');
		}else
		{

			this.distance = Math.sqrt(
				Math.pow((window.game.scene.player.frame.center().x - this.frame.center().x),2) +
				Math.pow((window.game.scene.player.frame.center().y - this.frame.center().y),2)
			);

			if (this.distance < this.activationDistance)
			{
				this.moving = true;

				var relX = window.game.scene.player.frame.center().x - this.frame.center().x;
				var relY = window.game.scene.player.frame.center().y - this.frame.center().y;

				this.rotation = Math.atan2(relY, relX);

				this.velocity = new Vector(
					Math.round((this.speed * Math.cos(this.rotation)) * 100) / 100,
					Math.round((this.speed * Math.sin(this.rotation)) * 100) / 100
				);

				if(this.distance < this.frame.size.width)
				{
					this.velocity.x = this.velocity.x * -2;
					this.velocity.y = this.velocity.y * -2;
				}

				// check for collisions
				var newOrigin = new Vector(this.frame.center().x, this.frame.center().y);
				newOrigin.add(this.velocity);

				var tile = window.game.scene.world.findTile(newOrigin);

				if (!tile.solid)
				{
					this.frame.origin.add(this.velocity);
				}else {
					this.velocity = new Vector(0,0);
				}

				this.enemySpriteSheet.setAnimation('walk');

			}else {
				this.moving = false;
				this.velocity = new Vector(0,0);
				this.enemySpriteSheet.setAnimation('idle');
			}
		}
	}


	this.draw = function(ctx)
	{
		this.buffer.clear();

		this.buffer.ctx.save();

		this.buffer.ctx.translate(this.buffer.center().x, this.buffer.center().y);
		this.buffer.ctx.rotate(this.rotation);

		this.buffer.ctx.drawImage(
			this.enemySpriteSheet.currentFrame(),
			(this.frame.size.width / 2) * -1,
			(this.frame.size.height / 2) * -1
		);

		this.buffer.ctx.restore();

		ctx.drawImage(
			this.buffer.canvas,
			this.frame.origin.x - (this.buffer.padding.width / 2),
			this.frame.origin.y - (this.buffer.padding.height / 2)
		);

		if (this.health > 0 && this.health < 100) {
			ctx.fillStyle = '#fff';
			ctx.fillText(this.health, this.frame.origin.x, this.frame.origin.y);
		}
	}

	this.collides = function(target)
	{
		if (this.alive && target.player)
		{
			target.health -= this.damage;
		}
	}
}
