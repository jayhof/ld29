var LucanusCrawlus = function(canvas)
{
	this.canvas = canvas;
	this.ctx = this.canvas.getContext('2d');

	this.buffer = null;
	this.bufferctx = null;

	this.interval = null;
	this.inveralID = null;

	this.scene = null;
	this.state = null;
	this.hud = null;
	this.resource = new ResourceManager();

	this.scenes = {
		menu: function() { return new MenuScene(); },
		game: function() { return new GameScene(); }
	}

	var self = this;


	this.init = function()
	{
		this.buffer = document.createElement('canvas');
		this.buffer.width = this.canvas.width;
		this.buffer.height = this.canvas.height;

		this.bufferctx = this.buffer.getContext('2d');

		this.state = new GameState();

		this.resource.loadImages([
			'game/resource/image/splash.png',
			'game/resource/image/world1.png',
			'game/resource/image/player.png',
			'game/resource/image/weapons.png',
			'game/resource/image/enemy1.png',
		], function()
		{
			self.hud = new Hud(new Frame(
				new Vector(0, self.canvas.height - 40),
				new Rectangle(self.canvas.width, 40)
			));

			self.hud.init();

			self.loadScene(self.scenes.menu());
		});
	}


	this.loadScene = function(scene)
	{
		self.stop();

		self.scene = null;
		self.scene = scene;

		self.scene.init(function() {
			self.start();
		});
	}


	this.start = function()
	{
		// var animFrame = window.requestAnimationFrame;

		// var recursiveAnim = function() {
		// 	self.tick();
		// 	animFrame(recursiveAnim);
		// }

		// animFrame(recursiveAnim);

		var self = this;
		var start, finish;

		this.intervalID = window.setInterval(function() {
			start = new Date();
			self.tick();
			finish = new Date();

			self.interval = 1000 / 60 - (finish - start);
		}, self.interval);
	}


	this.stop = function()
	{
		clearInterval(this.intervalID);
	}


	this.tick = function()
	{
		this.update();
		this.draw();
	}


	this.update = function()
	{
		this.state.update();
		this.scene.update();

		if (this.state.playing) {
			this.hud.update();
		}
	}


	this.draw = function()
	{
		this.bufferctx.clearRect(0,0,this.buffer.width,this.buffer.height);
		this.scene.draw(this.bufferctx);

		if (this.state.playing) {
			this.hud.draw(this.bufferctx);
		}

		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.drawImage(this.buffer, 0, 0);
	}
}
